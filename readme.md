# Pizza Test


## Local Deploy

Archive doesn't contain vendors and node_modules

Run `composer install`

Create local environment file `.env` . You can just rename `.env.example` and set proper credentials for DB.

If you don't have DB schema you can run `php artisan db-pizza:create`. To prepare DB run `php artisan migrate`

To seed data `php artisan db:seed`

Run `npm install` and then `npm run prod` or `npm run dev`

To run built-in development server on your localhost use `php artisan serve` command then open `http://localhost:8000`

## Tests

I added `IngredientServiceTest` just as an example of Unit test

To run all unit tests execute `./vendor/bin/phpunit`

To run certain test execute `vendor/bin/phpunit tests/Unit/IngredientServiceTest`

## About Pizza Test

As you have noticed in project we have Service layer that is responsible for business logic and getting data from DB. If I have more time I would add some kind of Manager layer that is for working with DB data,it's mostly for finding the data as ActiveRecord is used  

Having such layers allows us to have clear and flexible code. All code can be tested properly and independently.

For API routes Swagger ca be added