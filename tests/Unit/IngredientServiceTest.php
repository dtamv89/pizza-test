<?php

namespace Tests\Unit;

use App\Models\Ingredient;
use App\Services\IngredientService;
use Tests\TestCase;
use App;

class IngredientServiceTest extends TestCase
{
    public function testCreateHeroSuccessfully()
    {
        $ingredientMock = \Mockery::mock(Ingredient::class);
        $ingredientMock->shouldReceive('save')->once()->withAnyArgs()->andReturn(true);
        $ingredientMock->shouldReceive('fill')->once()->withAnyArgs()->andReturn(true);
        App::instance(Ingredient::class, $ingredientMock);

        $service = App::make(IngredientService::class);

        $service->create([]);
    }
}
