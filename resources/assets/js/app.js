import Vue from 'vue';
import VueRouter from 'vue-router';
import VueRes from 'vue-resource';
import App from './App.vue';
import Dashboard from './components/Dashboard.vue';
import AddPizza from './components/AddPizza.vue';
import EditPizza from './components/EditPizza.vue';
import AddIngredient from './components/AddIngredient.vue';
import ShowPizza from './components/ShowPizza.vue';

Vue.use(VueRouter)
Vue.use(VueRes)
const router = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'Dashboard',
            component: Dashboard
        }, {
            path: '/pizzas/add',
            name: 'AddPizza',
            component: AddPizza
        }, {
            path: '/pizzas/edit',
            name: 'EditPizza',
            component: EditPizza
        }, {
            path: '/ingredients/add',
            name: 'AddIngredient',
            component: AddIngredient
        }, {
            path: '/pizzas/:id',
            name: 'ShowPizza',
            component: ShowPizza
        }
    ]
});

Vue.router = router

Vue.url.options.root = 'http://localhost:8000/api'
App.router = Vue.router
new Vue(App).$mount('#app')
