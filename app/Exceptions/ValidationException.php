<?php

namespace App\Exceptions;

class ValidationException extends \Exception
{
    /**
     * @var
     */
    protected $errors;

    public function __construct($errors)
    {
        parent::__construct();
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }
}