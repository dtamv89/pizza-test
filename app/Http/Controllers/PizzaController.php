<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\PizzaService;
use App;
use App\Exceptions\ValidationException;
use App\Traits\ValidationTrait;
use App\Models\Pizza;

class PizzaController extends Controller
{
    use ValidationTrait;

    /**
     * @var PizzaService
     */
    protected $pizzaService;

    /**
     * PizzaController constructor.
     */
    public function __construct()
    {
        $this->pizzaService = App::make(PizzaService::class);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $pizzas = $this->pizzaService->getAll();

        return response()->json($pizzas);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $pizza = $this->pizzaService->get($id);

        return response()->json($pizza);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $this->validateData($request->all(), Pizza::$rules['create']);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $pizza = $this->pizzaService->create($request->all());

        return response()->json($pizza, Response::HTTP_CREATED);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $this->pizzaService->delete($id);

        return response()->json('', Response::HTTP_NO_CONTENT);
    }
}