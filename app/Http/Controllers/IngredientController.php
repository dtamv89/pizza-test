<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\IngredientService;
use App;
use App\Exceptions\ValidationException;
use App\Traits\ValidationTrait;
use App\Models\Ingredient;

/**
 * Class IngredientController
 * @package App\Http\Controllers
 */
class IngredientController extends Controller
{
    use ValidationTrait;

    /**
     * @var IngredientService
     */
    protected $ingredientService;

    /**
     * IngredientController constructor.
     */
    public function __construct()
    {
        $this->ingredientService = App::make(IngredientService::class);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $ingredients = $this->ingredientService->getAll();

        return response()->json($ingredients);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $this->validateData($request->all(), Ingredient::$rules['create']);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        }

        $ingredient = $this->ingredientService->create($request->all());

        return response()->json($ingredient, Response::HTTP_CREATED);
    }
}