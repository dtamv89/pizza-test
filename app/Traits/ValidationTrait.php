<?php

namespace App\Traits;

use App\Exceptions\ValidationException;
use Validator;

/**
 * Trait ValidationTrait
 * @package App\Traits
 */
trait ValidationTrait
{
    /**
     * @param $data
     * @param array $rules
     * @param array $messages
     * @throws ValidationException
     */
    public function validateData($data, array $rules, array $messages = [])
    {
        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors()->messages();
            throw new ValidationException($errors);
        }
    }
}



