<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db-pizza:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command creates DB schema if it doesn\'t exist.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connection = env("DB_CONNECTION") . ':host=' . env('DB_HOST') . ';port=' . env('DB_PORT');
        $pdo = new \PDO($connection, env('DB_USERNAME'), env('DB_PASSWORD'));
        $pdo->exec('CREATE SCHEMA IF NOT EXISTS ' . env("DB_DATABASE"));
    }
}
