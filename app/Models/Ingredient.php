<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ingredients';

    /**
     * @var array
     */
    protected $fillable = ['name', 'cost'];

    /**
     * Validation rules
     */
    public static $rules = [
        'create' => [
            'name' => 'required|string|unique:ingredients',
            'cost' => 'required|integer|min:0'
        ]
    ];
}