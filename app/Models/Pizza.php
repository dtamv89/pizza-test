<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pizzas';

    /**
     * @var array
     */
    protected $fillable = ['name', 'price'];

    /**
     * Include relations
     */
    protected $with = ['ingredients'];

    /**
     * Validation rules
     */
    public static $rules = [
        'create' => [
            'name' => 'required|string|unique:pizzas',
            'price' => 'required|integer|min:0'
        ]
    ];

    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class, 'pizzas_ingredients')->withPivot('quantity', 'order')
            ->orderBy('pizzas_ingredients.order', 'asc');
    }
}