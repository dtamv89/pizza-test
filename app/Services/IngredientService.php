<?php

namespace App\Services;

use App;
use App\Models\Ingredient;

/**
 * Class IngredientService
 * @package App\Services
 */
class IngredientService
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        return Ingredient::all();
    }

    /**
     * @param $data
     * @return Ingredient
     */
    public function create($data)
    {
        /** @var Ingredient $ingredient */
        $ingredient = App::make(Ingredient::class);
        $ingredient->fill($data);
        $ingredient->save();

        return $ingredient;
    }
}