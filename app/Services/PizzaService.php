<?php

namespace App\Services;

use App;
use DB;
use App\Models\Pizza;

class PizzaService
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        return Pizza::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return Pizza::find($id);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        /** @var Pizza $pizza */
        $pizza = App::make(Pizza::class);
        $pizza->fill($data);

        return DB::transaction(function () use ($pizza, $data) {
            $pizza->save();

            foreach ($data['ingredients'] as $ingredient) {
                if(empty($ingredient)) continue;
                $pizza->ingredients()->attach($ingredient['id'], ['order' => $ingredient['order'], 'quantity' => $ingredient['quantity']]);
            }

            return $pizza;
        });

    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $pizza = Pizza::find($id);
        $pizza->delete();
    }
}