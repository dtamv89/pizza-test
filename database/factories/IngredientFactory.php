<?php

use Faker\Generator as Faker;
use App\Models\Ingredient;

$factory->define(Ingredient::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'cost' => $faker->numberBetween(1,100)
    ];
});
