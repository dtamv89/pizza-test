<?php

use Faker\Generator as Faker;
use App\Models\Pizza;

$factory->define(Pizza::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => $faker->numberBetween(1,100)
    ];
});
