<?php

use Illuminate\Database\Seeder;
use App\Models\Pizza;
use App\Models\Ingredient;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Pizza::class, 5)->create()->each(function ($pizza) {
            $ingredient1 = factory(Ingredient::class)->create();
            $ingredient2 = factory(Ingredient::class)->create();

            $pizza->ingredients()->attach($ingredient1->id, ['order' => 1, 'quantity' => '10kg']);
            $pizza->ingredients()->attach($ingredient2->id, ['order' => 2, 'quantity' => '1kg']);
        });
    }
}
